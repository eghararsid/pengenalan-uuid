<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Otp_Code as Otp;
use App\Models\User;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $user = $request->user()->email_verified_at;

        if (!is_null($user)) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Anda Telah Melakukan Verifikasi Email!',
                'user' => $request->user()
            ], 200);
        }

        $otpBerhasil = Otp::where('kode', $request->kode)->first();

        if (!$otpBerhasil) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Kode OTP Anda Salah!'
            ], 200);
        }

        $now = Carbon::now();

        if ($now > $otpBerhasil->kadaluarsa) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Kode OTP Anda Kadaluarsa. Silahkan Regenerate OTP Baru!'
            ], 200);
        }

        $user = User::find($otpBerhasil->user_id);
        $user->email_verified_at = Carbon::now();
        $user->save();

        $otpBerhasil->delete();
        $data['user'] = $user;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Selamat! Akun Anda Telah Terverifikasi.',
            'data' => $data['user']
        ]);
    }
}
