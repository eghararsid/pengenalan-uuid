<?php

namespace App\Http\Controllers\Auth;

use App\Models\Otp_Code as Otp;
use App\Models\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Events\UserRegisteredEvent;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        $user = User::create([
            'name' => request('name'),
            'username' => request('username'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);

        Otp::create([
            'kode' => rand(100000,999999),
            'kadaluarsa' => Carbon::now()->addMinutes(15),
            'user_id' => $user->id
        ]);

        event(new UserRegisteredEvent($user));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Akun Berhasil Dibuat! Silahkan Verifikasi Email Anda',
            'User' => [
                'username' => $request->username,
                'name' => $request->name,
                'email' => $request->email,
                'created_at' => $user->created_at,
                'updated_at' => $user->updated_at,
                'id' => $user->id
            ]
        ]);
    }
}
