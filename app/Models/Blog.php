<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Blog extends Model
{
    use Uuid;

    protected $table = 'blogs';

    protected $guarded = [];
}
